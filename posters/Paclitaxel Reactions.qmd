---
title: "A National Service Evaluation of Paclitaxel Pre-medication Regimes for the Prevention of Hypersensitivity during a period of Ranitidine Shortage - initial data analysis"
authors:
  - name: Emma Foreman
    orcid: 0000-0002-4348-9040
  - name: Pinkie Chambers
    orcid: 0000-0002-6669-9411
  - name: Calum Polwart
    orcid: 0000-0002-4774-6366
date: 2021-10-09
citation:
  type: article-journal
  container-title: "Journal of Oncology Pharmacy Practice"
  issn: 1078-1552
  publisher: Sage Journals
  volume: 28
  issue: 2_suppl
  doi: "10.1177/10781552221078082"
  url: https://doi.org/10.1177/10781552221078082
bibliography: references.bib
categories: [BOPA, Poster, Adverse Reactions, Paclitaxel, H2-Antagonists]
---

This poster was presented by Emma Foreman at the British Oncology Pharmacy Association Annual Symposium. The poster summarised some work evaluating the impact of omitting ranitidine from paclitaxel infusions. Ranitidine was withdrawn from the market due to concerns about contamination. However, ranitidine had been widely considered an essential component of the premedication for paclitaxel to reduce the risk of allergic reactions.

Foreman, Chambers and myself collated data from a number of centres to evaluate the impact of using an alternative H2 antagonist or no antagonist. This work has subsequently been published in the [British Journal of Clinical Pharmacology](https://bpspubs.onlinelibrary.wiley.com/doi/epdf/10.1111/bcp.15363) as described on [this page of this site](articles/paclitaxel.html).

[![](images/paclitaxel.png){fig-alt="Paclitaxel allergy Poster"}](http://dx.doi.org/10.13140/RG.2.2.11080.85760/1)

The abstracts for this meeting are published in the[Journal of Oncology Pharmacy Practice](https://doi.org/10.1177/10781552221078082). The poster is available [on Research Gate](http://dx.doi.org/10.13140/RG.2.2.32944.89601).
