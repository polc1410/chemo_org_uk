---
title: "More High Dose Dexamethasone?"
description: |
  An update on high dose Dexamethasone.
author:
  - name: Calum Polwart
    orcid: 0000-0002-4774-6366
license: "This post is released under CC-BY-4.0 https://creativecommons.org/licenses/by/4.0/. Data shown is sourced from Open Prescribing, who obtain it from NHS BSA under an Open Government License."
copyright: "Ⓒ Calum Polwart 2024, Crown Copyright."
date: 2024-08-25
keywords:
  - Oncology
  - Pharmacy
  - Data
  - Open Source
  - Dexamethasone
categories:
  - OpenPrescribing
  - Data
  - Dexamethasone
citation:
  type: post-weblog
  container-title: "chemo.org.uk"
bibliography: biblio.bib 
google-scholar: true
format: 
    html: default
    aog-article-pdf: 
        papersize: a4
        geometry: margin=2cm
        colorlinks: true
        urlcolor: blue
        csl: ../apa.csl
shorttitle: High dose dexamethasone
shortauthors: Polwart, C
reference-section-title: "References"
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
```

Just before the world stopped for the COVID-19 pandemic, I wrote [a blog post](https://www.thedatalab.org/blog/67/high-dose-dexamethasone/) on TheDataLab's website about High Dose Dexamethasone. I was worried that 40mg Dexamethasone seemed to be being dispensed in Primary Care. This is a **huge** dose and would normally only have use in Haemato-oncology as part of a handful of specialist cancer treatments. In fact, even in secondary care, I would be worried that the risk of dispensing error was too high to justify keeping this in stock.

Distracted by the pandemic I kept an eye on prescribing rates and noticed they were descending. Only last year I discovered Dexamethasone 40mg Tablets had been discontinued. We still don't know if these dispensing events we saw were real or were data anomalies. I suggested to Chloë Waterson that she might want to take a look at the data when she was looking to learn [R](https://www.r-project.org/); whereby she told me it was no more! She had a look at some [even scarier stuff](posters/Primary%20Care%20Prescribing.html) instead!@waterson2023

So here is a very brief look at what happened, complete with code for those who like a simple R project

## Get the Data from Open Prescribing

```{r}
#| echo: true
#| warning: false
#| message: false
require(tidyverse, warn.conflicts = F)
require(httr, warn.conflicts = F)
require(jsonlite, warn.conflicts = F)
DexId <- "0603020G0AACJCJ" # This is the product code for 40mg Dex on Open Rx

url <- glue::glue("https://openprescribing.net/api/1.0/spending/?code={DexId}&format=json")
response <- GET(url)

# translate the API response into a datatable
data <- fromJSON(rawToChar(response$content))

# translate dates as dates not text
data |>
    mutate(date = as.Date(date)) -> data

# show the data
DT::datatable(data)
```

## Graph the Data

```{r}
#| echo: true
#| warning: false
#| message: false
require(ggplot2)
data |>
    ggplot(aes(y=quantity, x=date)) +
    geom_col() +  # creates a basic column graph
    theme_bw() +  # cleaner theme
    ggtitle("Number of 40mg Dexamethasone tablets dispensed each month") +
    expand_limits(x=as.Date("2024-07-01")) + # stretch the x axis to show all the missing right hand data points
    annotate(   # label when the original blog happened
    'text',
    x = as.Date("2019-08-15"),
    y = 75,
    label = 'Blog\nPublished',
    fontface = 'bold', 
    size = 3.5,
    angle = 0
  ) +
  annotate(
    'curve',
    x = as.Date("2019-11-15"),
    y = 50,
    yend = 28,
    xend = as.Date("2019-11-15"),
    linewidth = 1,
    curvature = 0,
    arrow = arrow(length = unit(0.2, 'cm'))
  ) -> g1

print(g1)
```

We can repeat the process for the number of prescriptions dispensed instead of the number of tablets dispensed. And we can combine those using cowplot.

```{r}
#| echo: true
#| warning: false
#| message: false
data |>
    ggplot(aes(y=items, x=date)) +
    geom_col() +  # creates a basic column graph
    theme_bw() +  # cleaner theme
    ggtitle("Number of 40mg Dexamethasone prescriptions dispensed each month") +
    expand_limits(x=as.Date("2024-07-01"), y=20) + # stretch the x axis to show all the missing right hand data points
    annotate(   # label when the original blog happened
    'text',
    x = as.Date("2019-08-15"),
    y = 15,
    label = 'Blog\nPublished',
    fontface = 'bold', 
    size = 3.5,
    angle = 0
  ) +
  annotate(
    'curve',
    x = as.Date("2019-11-15"),
    y = 11,
    yend = 6.5,
    xend = as.Date("2019-11-15"),
    linewidth = 1,
    curvature = 0,
    arrow = arrow(length = unit(0.2, 'cm'))
  ) -> g2

print(g2)
```

```{r}
#| echo: true
#| warning: false
#| message: false
require(cowplot)

cowplot::plot_grid(g1, g2, ncol = 1)

```

So it definitely looks like I can stop worrying about 40mg Dexamethasone Tablets. But that, instantly made me wonder what happened to the other products I'd been a bit worried by - the 10mg capsule. Well it seems when the 40mg tablet was discontinued a 10mg and 20mg tablet that were launched at the same time![^1]

[^1]: There were also 4mg and 8mg tablets launched back when the 40mg tablet was launched. While they still have risks of dispensing errors, the consequential risks is lower and 4 and 8mg are doses that would be commonly prescribed in primary care.

## Other Tablet Strengths

A browse through the [dm+d directory](https://openprescribing.net/dmd/vmp) finds these possible suspects:

| Description                                   | BNF Code        |
|-----------------------------------------------|-----------------|
| Dexamethasone 10mg capsules                   | 0603020G0AABYBY |
| Dexamethasone 10mg soluble tablets sugar free | 0603020G0AACLCL |
| Dexamethasone 20mg soluble tablets sugar free | 0603020G0AACMCM |

: "Higher" Strength Dexamethasone Tablets

These products do still appear to be available, based on the dm+d directory at least. If we re-run the data download for each of these codes we can create a combined graph. If there was a genuine clinical use for the 40mg tablets, we might expect to see an increase in 20mg dispensing when the 40mg becomes unavailable.

### Get the data

```{r}
#| echo: true
#| warning: false
#| message: false

# Add a strength column to the original data
data |>
    mutate(strength = "40mg") -> data

ids = c("10mg" ="0603020G0AABYBY",
        "10mg" = "0603020G0AACLCL", 
        "20mg" = "0603020G0AACMCM")

# use a loop to repeat the download for each id
for ( DexId in ids ) {

    url <- glue::glue("https://openprescribing.net/api/1.0/spending/?code={DexId}&format=json")
    response <- GET(url)
    newdata <- fromJSON(rawToChar(response$content))
    newdata |>
        mutate(date = as.Date(date)) |>
        mutate(strength = names(ids[ids == DexId]))-> newdata
    
    data <- rbind(data, newdata)
}


url <- glue::glue("https://openprescribing.net/api/1.0/spending/?code={DexId}&format=json")
response <- GET(url)

# translate the API response into a datatable
newdata <- fromJSON(rawToChar(response$content))

# translate dates as dates not text
newdata |>
    mutate(date = as.Date(date)) -> newdata

# show the data
DT::datatable(data)
```

It looks like we do have some higher strength dispensing happening in 2024!

## Graph the new data

```{r}
data |>
    ggplot(aes(y=quantity, x=date)) +
    geom_col(aes(fill = strength)) +  
    theme_bw() +  # cleaner theme
    ggtitle("Number of Dexamethasone tablets dispensed each month") +
    expand_limits(x=as.Date("2024-07-01")) -> g3

print(g3)
```

That's certainly not a reassuring picture! While the majority of the use is 10mg, even 10mg is a relatively high dose for prescribing in primary care. The equivalent prednisolone dose for 10mg of dexamethasone is over 65mg.

```{r}

data |>
    ggplot(aes(y=items, x=date)) +
    geom_col(aes(fill = strength)) +  
    theme_bw() +  # cleaner theme
    ggtitle("Number of Dexamethasone prescriptions dispensed each month") +
    expand_limits(x=as.Date("2024-07-01")) -> g4

print(g4)

```

This graph is more curious. When 40mg tablets were being dispensed (up to the end of 2022), there was a median of `r median(data$items[data$strength == "40mg"], na.rm=T)` prescriptions per month for `r median(data$quantity[data$strength == "40mg"], na.rm=T)` tablets per month. Between `r format(min(data$date[data$strength == "40mg"], na.rm=T), "%B-%y")` and `r format(max(data$date[data$strength == "40mg"], na.rm=T), "%B-%y")`, `r sum(data$quantity[data$strength == "40mg"], na.rm=T) * 0.040` grams of dexamethasone was dispensed. If the 10mg and 20mg tablets directly replaced these, then a similar number of grams should be being dispensed in a similar time span. In fact, since the introduction of 20mg tablets, a median of `r median(data$items[data$strength != "40mg"], na.rm=T)` prescriptions per month and `r median(data$quantity[data$strength != "40mg"], na.rm=T)` tablets per month have been dispensed.

```{r}
data |>
    filter(date >= as.Date("2022-06-01") & date <= as.Date("2024-07-01")) |>
    group_by(strength) |>
    summarise('total tablets' = sum(quantity)) |>
    separate_wider_delim(strength,  delim = "mg", names= "mg", too_many = "drop") |>
    mutate(mg = as.numeric(mg)) |>
    mutate(grams = mg*`total tablets`/1000) -> tentwentyData
```

In the time period July-22 to June-24, `r sum(tentwentyData$grams)` grams of dexamethasone were dispensed, representing a `r  round(sum(tentwentyData$grams)/(sum(data$quantity[data$strength == "40mg"], na.rm=T) * 0.040), 0)` fold increase in the use of higher strength steroid tablets.

## Conclusion

The discontinuation of 40mg Dexamethasone Tablets resulted in a brief period without dispensing of any high strength dexamethasone prescriptions. While high doses may still have been dispensed using lower strength tablets, the potential to identify these as possibly erroneous is increased by the dispensing of large tablet counts to achieve high doses. However, following the introduction of 10 and 20mg tablets there now seems to be a `r  round(sum(tentwentyData$grams)/(sum(data$quantity[data$strength == "40mg"], na.rm=T) * 0.040), 0)` fold increase in the dispensing of high doses of steroids using higher strength tablets. **I am not reassured!**
