---
title: "British Oncology Pharmacy Association Delphi consensus guidelines: Co-infusion of trometamol-containing calcium folinate (Leucovorin) with systemic anti-cancer treatments"
authors:
  - name: Calum Polwart
    orcid: 0000-0002-4774-6366
  - name: Tim Root
  - name: Songül Tezcan
  - name: Sharon Meehan 
  - name: Bill Wetherill
  - name: Chloë Waterson 
    orcid: 0009-0000-7877-1954
  - name: Bruce Burnett 
  - name: Rena Chauhan 
  - name: Ibrahim Al-Modaris 
date: 2024-04-05
preview: images/BOPA-Oxali.png
citation:
  type: article-journal
  container-title: "Journal of Oncology Pharmacy Practice"
  doi: "10.1177/10781552241243360"
  url: https://journals.sagepub.com/doi/10.1177/10781552241243360
license: "Restricted access"
copyright: "© 2024 The Authors."
categories: [Journal Article, Consensus Guidelines, Oxaliplatin, Trometamol]
bibliography: references.json
---

This publication followed development of a set of guidelines for hospitals wishing to give Oxaliplatin in combination with Trometamol containing Folinic Acid. Oxaliplatin is routinely co-infused with folinic acid (known as Leucovorin in the US and Canada). All manufacturers of Oxaliplatin state that this is safe to do, *provided* the folinic acid does not contain trometamol as an excipient.

Until recently, folinic acid containing trometamol has not been available. However, supply shortages of trometamol free folinic acid have seen the introduction of a generic folinic acid that contains trometamol.

[BOPA](https://bopa.org.uk) was asked to consider if there was a significant risk from co-infusion. Back in 2022 I first met with a group of interested colleagues and we discussed various options. We didn't make much progress as there doesn't seem to be much explanation for the need to avoid trometamol. I attended the CaPhO Conference in 2023 and happened upon a poster addressing the very same issue.

[![](images/SunnBrookPoster.png){fig-alt="SunnyBrook's Poster"}](https://www.metrodis.org/uploads/document_library/64eea5e5fea3db84cee8f7137f59426c.pdf)

Their poster @char seemed to show there isn't an issue at normal concentrations. We convened a meeting to summarise these results, to which Nathan Ma (one of the poster authors) kindly attended. Following that we undertook a Delphi Consensus Processs to form a Consensus for the UK.

[![](images/BOPA-Oxali.png)](https://journals.sagepub.com/doi/10.1177/10781552241243360)

This article is restricted access. However, all members of the [International Society Oncology Pharmacy Practitioners (ISOPP)](https://www.isopp.org/) receive free access to the Journal. ISOPP is open to membership of any oncology pharmacist world wide *and membership is currently free*.


<span class="__dimensions_badge_embed__" data-doi="10.1177/10781552241243360" data-style="large_rectangle"></span> <div data-badge-popover="right" data-badge-type="1" data-doi="10.1177/10781552241243360" data-hide-no-mentions="true" class="altmetric-embed"></div>

<script type='text/javascript' src='https://d1bxh8uas1mnw7.cloudfront.net/assets/embed.js'></script>

<script async src="https://badge.dimensions.ai/badge.js" charset="utf-8"></script>