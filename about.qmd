---
title: "About"
image: profile.jpg
about:
  template: trestles
  links:
    - icon: twitter
      text: Twitter
      href: https://twitter.com/shinyblackshoe
    - icon: chat-heart-fill
      text: BSky.Social
      href: https://bsky.app/profile/shinyblackshoe.bsky.social
    - icon: linkedin
      text: LinkedIn
      href: https://www.linkedin.com/in/calum-polwart-1520b216b/
    - icon: github
      text: Github
      href: https://github.com/polc1410
    - icon: gitlab
      text: Gitlab
      href: https://github.com/polc1410
---

## Bio

Calum Polwart is a Consultant Pharmacist who specialises in treatment of cancer. He has a strong interest in information technology and data processing.

## Education

**Univeristy of Birmingham** \| Birmingham, UK

MSc (with Distinction) in Clinical Oncology \| 2002-2003

**University of Strathclyde** \| Glasgow, UK

BSc (Hons) in Pharmacy \| September 1994 - June 1998

## Experience

**South Tees NHS Foundation Trust** \| Consultant Pharmacist \| March 2019 - Present

**County Durham & Darlington NHS Foundation Trust** \| Tees Valley Regional Cancer Pharmacist (Secondment) \| April 2017 - March 2019

**NHS England** \| Cancer Commissioning Pharmacist \| April 2013 - March 2016, April 2017 - Present

**Northern Cancer Network** \| Cancer Network Pharmacist \| April 2008 - March 2013

**County Durham & Darlington NHS Foundation Trust** \| Lead Pharmacist - Cancer and Aseptic Services \| June 2005 - April 2017

**Cancer Care Alliance of Teesside** \| Cancer Network Pharmacist \| June 2005 - April 2008

**South Tyneside NHS Foundation Trust** \| Lead Pharmacist - Cancer and Aseptic Services \| August 2000 - May 2005

**South Tyneside NHS Foundation Trust** \| Resident Clinical Pharmacist \| August 1999 - July 2000
