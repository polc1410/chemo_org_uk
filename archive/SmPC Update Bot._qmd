---
title: "SmPC Update Bot"
description: |
  About the creation of a bot that will detect changes in Summary of Product Characteristics(SmPCs) for medicines that I have an interst in
author:
  - name: Calum Polwart
    orcid_id: 0000-0002-4774-6366
date: 2022-07-16
bibliography: references.bib
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
```

## Background

About 5 years ago I was made aware of a requirement in the "Quality Assurance of Aseptic Preparation Services: Standards" (@quality2016 ) that there is a need to verify the Summary of Product Characteristics (SmPC) has not changed since the previous supply.

> 13.1.4 For licensed starting materials, systems for receipt should also include verification that the Summary of Product Characteristics or technical information supplied has not changed since the previous receipt. Where changes are noted, there should be an impact assessment conducted and if the change requires a modification then change management procedures should be invoked.

While this is a very laudable requirement, the concept that every time you receive a delivery of 10 vials of a drug they will be placed into quarantine until someone checks the SmPC version number seems a little far fetched. Most units get around this by 'periodically' checking the SmPCs online for any changes. Thats a lot of SmPC's to scroll through, rarely finding any changes. No aseptic unit ever has spare staff, and so this is a job that gets left. But, if there is a need (I'd challenge the QA community to tell me of a time a change like this came un-notified from manufacturers), that means the period when there could be a risk increases.

It was obvious this should be automated. The [Electronic Medicines Compendium](https://medicines.org.uk) has some functions that could help, including the ability to bookmark SmPCs of interest. However, there is no mechanism in the website to pro-actively be notified of changes. So 5 years ago I set about writing a bot in python that could find and notify changes. It ran as an alpha version on the chemo.org.uk server sending me emails when it found changes, or more often emails to say it had crashed. Having moved further from the production side, sadly I lost the drive to move the project to a stable beta version and my intention had simply been to wind up the alpha version as I re-structure the chemo.org.uk server.

In the last month, I've come across two SmPC changes I'd have liked to have known about sooner. One, a change of clinical licence that occurred back in January with the addition of a Breast Cancer licence to 3 monthly [gozerelin](https://www.medicines.org.uk/emc/product/1567/smpc), and most recently a change in licenced shelf life for [sactituzumab govitecan](https://www.medicines.org.uk/emc/product/12880/smpc). In sacituzumab govitecan's case I'd just finished writing some messages about planning its introduction, and the need to plan how to handle its short shelf life, when a colleague highlighted the change had occurred a week earlier. *It made me think we need a system to know about any SmPC changes*, and it just so happens I had an 'oven-ready' solution.

## Solution

### Concept

Except, as I've alluded to above, my 'oven-ready' solution was about as 'over-ready' as a certain political deal. I was using a rather complex process, written in python that I barely understood. I've grown to love R as a programming language, and I had no doubt I could re-write the web-crawl part in R using the [rvest](https://www.rdocumentation.org/packages/rvest/) package (@rvest-package ). The python version used email to communicate that a change had occurred, but I now felt something better could be done. If the 'bot' could generated a tweet, anyone interested could follow the 'bot' account on twitter, and with the help of the 'alarm bell' notification on twitter they could even be actively notified of a tweet.

In addition, the list of tweets would be available for later review. I could also create a tweet to announce that a particular SmPC would now be followed (or potentially un-followed). The [rtweets](https://docs.ropensci.org/rtweet/) would be the solution to that (@rtweet-package ).

### Hosting

Hosting a bot doesn't necessarily take a huge amount of computing power. It would certainly have been within the capabilities of chemo.org.uk's former server. But the power consumption on that server, both financially and environmentally is becoming hard to justify. I've moved chemo.org.uk's website to [Git Lab Pages](https://docs.gitlab.com/ee/user/project/pages/) - which is where you are reading this page now.

The bot would of course be placed into a git repository on github or gitlab. Git Lab has the ability to run 'docker' sessions on a schedule or on git comits. For those who are thinking this is getting too deep, basically, as long as the computing power is kept reasonable, the server can host the bot.

## How to use

### Simple update alerts

The simplest way to use this bot is to follow its [twitter page](https://twitter.com/SPCChemo) using your own twitter account. Its free. It won't be posting about anything else and so you can stay fairly immune to twitter's evils if you want.

[![Twitter Profile Page highlighting Alert Button and Follow Button](../images/TwitterPic.png)](https://twitter.com/SPCChemo)

### Adding SmPCs

If you think there is an SmPC the bot should be following - let me know (I'd suggest a message on twitter? Or open an issue on [GitLab](https://gitlab.com/polc1410/smpccrawl/-/issues). If you feel up to it, you could submit a change through git to the file in the repository [Data/spc_list.csv](https://gitlab.com/polc1410/smpccrawl/-/main/Data/spc_list.csv).

I'm only intending this bot to be interested in Systemic Anti-Cancer Treatments. I'm considering adding other things to track - I'd be interested to know what else you struggle to know has changed? NICE Appraisal Updates perhaps?

### Creating your own bot

If you want to create a similar bot for another therapeutic area, you could fork the project from git lab and create your own version. The hardest part is getting the twitter developer account to give you the right access.
